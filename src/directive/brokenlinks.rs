use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::Site;
use crate::wikitext::ParsedDirective;

#[derive(Debug, Default, Eq, PartialEq)]
pub struct Brokenlinks {}

impl DirectiveImplementation for Brokenlinks {
    const REQUIRED: &'static [&'static str] = &["pages"];
    const ALLOWED: &'static [&'static str] = &[];
    const ALLOW_ANY_UNNAMED: bool = false;

    fn from_parsed(_: &ParsedDirective) -> Self {
        Self::default()
    }

    fn process(&self, _site: &Site, _meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        // We don't allow broken links, but we want to allow the
        // directive, so this is very simple.
        Ok(Processed::Markdown("".into()))
    }
}
