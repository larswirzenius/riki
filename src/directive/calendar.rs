use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::Site;
use crate::wikitext::ParsedDirective;

#[derive(Debug, Default, Eq, PartialEq)]
pub struct Calendar {}

impl DirectiveImplementation for Calendar {
    const REQUIRED: &'static [&'static str] = &[];
    const ALLOWED: &'static [&'static str] = &[
        "type",
        "pages",
        "year",
        "month",
        "week_start_day",
        "months_per_row",
        "archivebase",
    ];
    const ALLOW_ANY_UNNAMED: bool = true;

    fn from_parsed(_: &ParsedDirective) -> Self {
        Self::default()
    }

    fn process(&self, _site: &Site, _meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        Err(DirectiveError::UnimplementedDirective("calendar".into()))
    }
}
