use crate::pagespec::{Expr, OpCode};

grammar;

pub Expr: Box<Expr> = {
    Expr Op Term => Box::new(Expr::Op(<>)),
    Term,
}

Term: Box<Expr> = {
    Glob => Box::new(Expr::Glob(<>)),
    "link" "(" <g:Glob> ")" => Box::new(Expr::LinksHereFunc(<>)),
    "page" "(" <g:Glob> ")" => Box::new(Expr::PageFunc(<>)),
    "tagged" "(" <g:Glob> ")" => Box::new(Expr::TaggedFunc(<>)),
    "!" <t:Term> => Box::new(Expr::Negate(t)),
    "(" <e:Expr> ")" => e,
}

Glob: String = {
    // Ideally we would use \w below, instead of Latin letter ranges, but
    // as of 2023-04-22 it fails to compile.
    r"([a-zA-Z0-9._*?/-])+" => <>.to_string(),
}

Op: OpCode = {
    "and" => OpCode::And,
    "or" => OpCode::Or,
}
