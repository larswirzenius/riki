use crate::html::HtmlError;
use std::path::PathBuf;

#[derive(Debug, thiserror::Error)]
pub enum RikiError {
    #[error(transparent)]
    WalkDir(#[from] crate::srcdir::SourceDirError),

    #[error(transparent)]
    Util(#[from] crate::util::UtilError),

    #[error(transparent)]
    Git(#[from] crate::git::GitError),

    #[error(transparent)]
    Page(#[from] crate::page::PageError),

    #[error(transparent)]
    Directive(#[from] crate::directive::DirectiveError),

    #[error(transparent)]
    Parser(#[from] crate::parser::ParserError),

    #[error(transparent)]
    Time(#[from] crate::time::TimeError),

    #[error(transparent)]
    Version(#[from] crate::version::VersionError),

    #[error(transparent)]
    PageSpec(#[from] crate::pagespec::PageSpecError),

    #[error(transparent)]
    Wikitext(#[from] crate::wikitext::WikitextError),

    #[error(transparent)]
    HtmlError(#[from] HtmlError),

    #[error(transparent)]
    Site(#[from] crate::site::SiteError),

    #[error("failed to write file {0}")]
    WriteFile(PathBuf, #[source] std::io::Error),
}
